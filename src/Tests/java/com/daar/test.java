package com.daar;

import com.daar.request.QueryRequest;
import com.daar.request.Document;
import lombok.extern.slf4j.Slf4j;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import java.io.IOException;
import java.util.List;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@ActiveProfiles("test")
@Slf4j
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class test {

    @Autowired
    private QueryRequest req;

    private Document doc = new Document(null, "fouzmou", "je ne sais pas");

    @org.junit.Test
    public void testA() {
        assertNotNull(req.indexRequest(doc));
    }

    @org.junit.Test
    public void testB() {
        assertFalse(req.matchAllQuery().isEmpty());
    }

    @org.junit.Test
    public void testC(){
        assertFalse(req.matchQuery("je").isEmpty());
    }

    @org.junit.Test
    public void testD() throws IOException {
        final List<Document> documentList = req.matchAllQuery();
        documentList.forEach(doc -> req.deleteDocument(doc.getId()));
        req.refreshRequest();
        assertTrue(req.matchAllQuery().isEmpty());
    }
}