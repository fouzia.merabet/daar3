function fillTable(){

    $("#myTable").find("tr:gt(0)").remove();
    $.ajax({
        type: "GET",
        url: "/api/all",
        contentType: "application/json; charset=utf-8",
        success: function(result) {
            result.forEach(function (obj) {
                $("#myTableBody").append("<tr><td>" + obj.title + "</td>"
                    + "<td>"
                    + "  <button type=\"button\" onclick=\"deleteDocument(\'" + obj.id + "\')\" class=\"btn btn-danger\">Delete</button></td>");
            });
        },
        error: function(err) {
            alert(JSON.stringify(err, null, 2));
        }
    });
}
 function getBase64(file) {
     let reader = new FileReader();
     reader.readAsDataURL(file);
     reader.onload =  function () {
         let content =reader.result.replace(/^data:.+;base64,/, '');
         let id =  $("#docId").val();
         let document = {
             id: id,
             title: file.name,

             content:content
         };

         sendToWS(document);
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
}
function sendToWS(document){
    event.preventDefault();
    let url = "/api/create";
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(document),
        contentType: "application/json; charset=utf-8",
        success: function () {
            $(clearFields());
            $(fillTable());
        },
        error: function(err) {
            alert(JSON.stringify(err, null, 2));
        }
    });
}

function deleteDocument(id) {
    $.getJSON("/api/delete/", {id: id});
    alert("Your document has been deleted.");
    $(fillTable());
}

function fillFields(id, title, content) {
    $("#docId").val(id);
    $("#title").val(title);
    $("#content").val(content);
}

function clearFields() {
    $("#docId").val("");
    $("#title").val("");
    $("#content").val("");
    $("#query").val("");
}

$(document).ready(function() {
    $(fillTable());
    $( "#query" ).keyup(function() {
        $.getJSON("/api/search/", {query: $("#query").val()} , function(result) {
            $("#myTable").find("tr:gt(0)").remove();
            result.forEach(function (obj) {
                $("#myTable > tbody:last").after( "<tr><td>" + obj.title + "</td>"
                    + "<td>"
                    + "  <button type=\"button\" onclick=\"deleteDocument(\'" + obj.id + "\')\" class=\"btn btn-danger\">Delete</button></td>")
            });
        });
    });
    $("#save").click(function(){
        event.preventDefault();
        let file =$("#fileUpload")[0].files[0];
        getBase64(file);
    });
});