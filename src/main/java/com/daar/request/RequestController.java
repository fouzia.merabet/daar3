package com.daar.request;

import com.google.common.io.Files;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


@RestController
public class RequestController {

    private final QueryRequest req;

    public RequestController(final QueryRequest req) {
        this.req = req;
    }

    @PostMapping(value = "/api/create", consumes = "application/json; charset=utf-8")
    public String create(@RequestBody Document document ) throws IOException {

        String base64 = document.getContent();
        String title= document.getTitle();
        String ext =Files.getFileExtension(title);
        byte[] decoded = java.util.Base64.getDecoder().decode(base64);
        String strippedText;
        if (ext.equals("pdf")){
            PDDocument doc = PDDocument.load(decoded);
            PDFTextStripper stripper = new PDFTextStripper();
            strippedText = stripper.getText(doc);

        }else{
            if (ext.equals("docx")){
                InputStream stream = new ByteArrayInputStream(decoded);
                XWPFDocument docx = new XWPFDocument(stream);
                XWPFWordExtractor we = new XWPFWordExtractor(docx);
                strippedText= we.getText();
            }else {
                strippedText="";
            }
        }

        document.setContent(strippedText);

        return req.indexRequest(document);
    }

    @GetMapping(value = "/api/delete")
    public void delete(String id) {
        req.deleteDocument(id);
    }


    @GetMapping(value = "/api/all", produces = "application/json; charset=utf-8")
    public List<Document> getAllDocuments() {
        return req.matchAllQuery();
    }

    @GetMapping("/api/search")
    public List<Document> search(@RequestParam("query") String query) {
        return req.matchQuery(query);
    }


}
