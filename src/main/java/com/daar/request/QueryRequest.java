package com.daar.request;

import com.daar.prop.ConfigProps;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.AbstractQueryBuilder;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class QueryRequest {

    private final RestHighLevelClient client;
    private final SearchSourceBuilder sourceBuilder;
    private final ConfigProps props;
    private final Gson gson;

    @Autowired
    public QueryRequest(RestHighLevelClient client, SearchSourceBuilder sourceBuilder,
                        ConfigProps props, Gson gson) {
        this.client = client;
        this.sourceBuilder = sourceBuilder;
        this.props = props;
        this.gson = gson;
    }


    public String indexRequest(final Document document) {

        try {
            final IndexRequest indexRequest = new IndexRequest(props.getIndex().getName())
                    .id(document.getId())
                    .source(XContentType.JSON, "title", document.getTitle(),
                            "content", document.getContent());
            final IndexResponse response = client.index(indexRequest, RequestOptions.DEFAULT);
            return response.getId();

        } catch (Exception ex) {
            log.error("Exception : createIndex method.", ex);
        }

        return null;
    }



    public List<Document> matchAllQuery() {

        List<Document> result = new ArrayList<>();

        try {
            refreshRequest();
            result = getDocuments(QueryBuilders.matchAllQuery());
        } catch (Exception ex) {
            log.error("Exception : matchAllQuery method.", ex);
        }

        return result;
    }


    public List<Document> matchQuery(String query) {
        List<Document> result = new ArrayList<>();
        BoolQueryBuilder query1 = QueryBuilders.boolQuery();
        query = query.replaceAll("\\s", "");
        String[] str = query.split("&");
        for (int i = 0; i < str.length; i++) {
            query1 = query1.must(QueryBuilders.queryStringQuery("*" + str[i].toLowerCase() + "*"));
        }


        BoolQueryBuilder builder = QueryBuilders.boolQuery().filter(query1);
        try {

            result = getDocuments(builder);
        } catch (Exception ex) {
            log.error("Exception : matchQuery method.", ex);
        }

        return result;
    }


    public void deleteDocument(String id) {
        try {
            final DeleteRequest delete = new DeleteRequest(props.getIndex().getName(), id);
            client.delete(delete, RequestOptions.DEFAULT);
        } catch (Exception ex) {
            log.error("Exception : deleteDocument method.", ex);
        }
    }


    private SearchRequest getSearchRequest() {
        SearchRequest request = new SearchRequest(props.getIndex().getName());
        request.source(sourceBuilder);
        return request;
    }

    private List<Document> getDocuments(AbstractQueryBuilder builder) throws IOException {
        List<Document> result = new ArrayList<>();
        sourceBuilder.query(builder);
        SearchRequest request = getSearchRequest();
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        SearchHits hits = response.getHits();
        SearchHit[] searchHits = hits.getHits();
        for (SearchHit hit : searchHits) {
            Document doc = gson.fromJson(hit.getSourceAsString(), Document.class);
            doc.setId(hit.getId());
            result.add(doc);
        }

        return result;
    }

    public void refreshRequest() throws IOException {
        final RefreshRequest request = new RefreshRequest(props.getIndex().getName());
        client.indices().refresh(request, RequestOptions.DEFAULT);
    }
}
